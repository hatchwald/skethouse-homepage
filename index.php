<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home Skesthouse</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>

<body>

    <div class="container">
        <div class="row" style="margin-top: 50px;">
            <div class="col-md-12">
                <?php
                include("./get_data.php");
                if ($data_arr['status'] != 200) {
                ?>
                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Error!</h4>
                        <p>Error At fetch data from Server, reload this page again or check your connection.</p>
                        <hr>
                        <p class="mb-0">Status Error Code <?php echo $data_arr['status']; ?></p>
                    </div>
                <?php
                } else {

                ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-start">
                                <h3>List Of Comment</h3>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-end">
                                <a href="#" class="btn btn-info" id="btn_create_new">Create New Comment</a>
                            </div>
                        </div>
                    </div>
                    <table class="table" id="table_datas">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Comment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            foreach ($data_arr['data'] as $value) {
                                $i++;
                            ?>
                                <tr>
                                    <th scope="row"><?php echo $i; ?></th>
                                    <td><?php echo $value['name']; ?></td>
                                    <td><?php echo $value['comments']; ?></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                <?php
                }
                ?>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal_comment" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal_commentLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_commentLabel">Create New Comment</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="" id="form_comments">
                        <fieldset>
                            <div class="mb-3">
                                <label for="" class="form-label">
                                    Name
                                </label>
                                <input type="text" class="form-control" placeholder="Type your name" name="nama" required>
                            </div>
                            <div class="mb-3">
                                <label for="" class="form-label">
                                    Comment
                                </label>
                                <textarea id="" cols="30" rows="10" class="form-control" name="comments" required></textarea>
                            </div>
                        </fieldset>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" id="submit_btn" class="btn btn-primary">Send Comment</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Toast -->

    <div class="position-fixed top-0 end-0 p-3" style="z-index: 5">
        <div id="some_toast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <strong class="me-auto">Notification</strong>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body">

            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="./script.js"></script>
</body>

</html>