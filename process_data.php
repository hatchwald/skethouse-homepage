<?php
require __DIR__ . '/vendor/autoload.php';

use GuzzleHttp\Client;

$client = new Client();
$datas = ["name" => $_POST['nama'], "comments" => $_POST['comments']];
$datas = json_encode($datas);
$response = $client->request("POST", "https://sketsahouse-stagging.herokuapp.com/comment", [
    "form_params" => [
        "name" => $_POST['nama'],
        "comments" => $_POST['comments']
    ]
]);

$body_data = $response->getBody();
$status_code = $response->getStatusCode();
$data_content = $body_data->getContents();

if ($status_code != 200) {
    echo "error";
} else {
    echo "success";
}
