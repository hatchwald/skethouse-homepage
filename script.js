$(document).ready(function () {
    $("#btn_create_new").on("click", () => {
        $("#modal_comment").modal("show")
    })

    $("#form_comments").on("submit", function (e) {
        e.preventDefault()
        $("#submit_btn").prop("disabled", true)
        const length_table = $("#table_datas tbody tr").length
        const datas = $(this).serializeArray();
        $("#form_comments fieldset").prop("disabled", true)
        // console.log(data[0].value)
        // return;
        $.ajax({
            data: datas,
            url: "process_data.php",
            method: "POST"

        }).done(data => {
            $("#modal_comment").modal("hide")
            $("#table_datas tbody").append(`<tr>
            <th scope="row">${length_table + 1}</th>
            <td>${datas[0].value}</td>
            <td>${datas[1].value}</td>
        </tr>`)
            $("#some_toast .toast-body").html("Success Send Comment")
            $("#some_toast").toast("show")

        }).fail(err => {
            $("#some_toast .toast-body").html("An Error was happen , check log")
            $("#some_toast").toast("show")
            console.log(err)
        })
    })

    $("#modal_comment").on("hide.bs.modal", function () {
        $("#form_comments fieldset").prop("disabled", false)
        $("#submit_btn").prop("disabled", false)
        $("#form_comments").trigger("reset")
    })
})