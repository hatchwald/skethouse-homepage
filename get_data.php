<?php
require __DIR__ . '/vendor/autoload.php';

use GuzzleHttp\Client;

$client = new Client();
$request_data = $client->request('GET', "https://sketsahouse-stagging.herokuapp.com/comment/alldata");
$body_data = $request_data->getBody();
$status_code = $request_data->getStatusCode();
$data_content = $body_data->getContents();
$data_content = json_decode($data_content, true);
$data_arr =  ["status" => $status_code, "data" => $data_content];
